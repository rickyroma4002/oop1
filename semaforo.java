
public class semaforo {
    String[] colore = new String[] {"Verde", "Giallo", "Rosso"};
    int i;
    double intervallo=0;
    double tempo=0;

    public semaforo(String colore, double intervallo){
        if (colore.equals("Verde"))i=0;
        if (colore.equals("Giallo"))i=1;
        if (colore.equals("Rosso"))i=2;
        this.intervallo=intervallo;
        tempo=intervallo;
    }
    public String toString(){
        return "semaforo\n" +
                "colore attuale == "+colore[i]+"\n"+
                "intervallo == "+intervallo+"\n";
    }
    public void aspetta(double quanto){
        if (tempo-quanto<=0) {
            tempo-=quanto;
            if (i==2)i=0;
            else i++;
            tempo+=intervallo;
        }
        else{
            tempo-=quanto;
        }
    }
    public String colore_att(){
        return colore[i];
    }
}
