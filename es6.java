import java.util.*;

public class es6{
    public static void main(String[] args){
        semaforo sem1= new semaforo("Rosso", 10);   //colore iniziale e intervallo in sec.
        int time;
        Scanner in = new Scanner(System.in);
        while (true){
            System.out.println("Colore attuale: "+sem1.colore_att());      //stampa colore attuale
            System.out.println("Inserisci quanto tempo aspettare:\n");
            time=in.nextInt();in.nextLine();
            sem1.aspetta(time);   //quanto tempo aspetti in sec.

        }
    }
}